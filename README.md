# How to install and build:

- Step 1: download/clone project from following way:

 ### clone

 https://gitlab.com/akhileshcoder/teeter-totter.git

 ### download and unzip:

[https://drive.google.com/open?id=1dtqFNp6Tj6Hbqc50X10gdfyaD91HMr93](https://drive.google.com/open?id=1dtqFNp6Tj6Hbqc50X10gdfyaD91HMr93)

- Step 2: run following command in root directory and wait till finish

`npm i && npm run start`

- Step 3: open following link in browser:

http://localhost:8080

> if 8080 port is not free it will console which port you should visit.

